<?php

return [
    // Is the application in production?
    // This is used for sereval optimizations for the PHP-DI container, the Twig
    // debug extension and routing
    'Production'        => env('PRODUCTION', false),
    
    // The locale to use for carbon
    'Locale'            => 'nl',

    // Services configured for this application
    'Services'          => [
        \Framework\Service\Database\DatabaseService::class
    ],

	/*
	 * The event dispatcher to be used.
	 * The one provided by default enables the '*' listener name, which makes
	 * a listener or subscriber listen to all events.
	 *
     * The '*' listener is used within the framework, so the dispatcher specified
     * should support this functionality.
	 */    
    'EventDispatcher'	=> \Framework\Event\EventCarrier::class,
    
    // The PHP time zone.
    'Timezone'          => 'Europe/Amsterdam',

    // Application URL required for sending information to users where a full link is required.
    // like in e-mails. Do not use a backslash on the end of the Url
    'Url' => env('URL', 'http://webarray.nl'),

    // Loggin configuration
    'Logging' => [
        'File'  => '@ROOT/Storage/User/error.log'
    ],

    // Show fancy and more helpful error pages
    // Setting this to 'no' will also prevent an error handler from being registered
    'ShowWhoops'        => true,
    
    // Path to the user storage directory
    'StorageDir'        => '@ROOT/Storage/User',

    // Path to the user storage directory
    'PublicDir'        => '@ROOT/public',
    
    // Path to the directory for file uploads
    // This path is relative under StorageDir.
    // So: StorageDir/UploadDir
    // This should start with a '/'
    'UploadDir'         => '/Content',
    
    // The name of the 'remember me' cookie
    'RememberMeCookie'  => 'remember_me',
    'Key'               => 'ae47f9f692bcb1793c684b6039395113',
    'HashOptions'       => [
        'cost'  => 11
    ]
];