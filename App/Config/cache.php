<?php

return [
    // Should routes be cached?
    // This can be slower when there are a few route entries
    // that mostly consist of functions as actions.
    // When a moderate amount of routes with controllers are defined, set this to yes
    // for production.
    'Routes'        => env('CACHE_ROUTES', false),
    
    // Route cache entry name
    'RoutesName'    => 'routes.cache',
    
    // Should Twig cache templates? Should be 'no' in development
    'Templates'     => env('CACHE_TEMPLATES', false),
    
    // Path to the template cache directory for Twig
    'TemplateDir'   => '@ROOT/Storage/Cache/Template',

    // Cache configuration for PHP-DI
    'PHP-DI'        => [
        // What type of caching to use. Options:
        // Array
        // File
        // Redis
        // If left empty, no caching will be used.
        'Storage'   => 'Array'
    ],

    // Configuration for the Cache Provider
    'FastCache'     => [
        
        // What medium to use for storage. Options:
        // ssdb
        // predis
        // redis
        // mongodb
        // files
        // sqlite
        // auto
        // apc
        // wincache
        // xcache
        // memcache
        // memcached
        'storage'       => 'files',

        // Server information for server-based cache
        'predis'         => [
            'host'          => '127.0.0.1',
            'port'          => '6379',
            'username'      => '',
            'password'      => '',
            'timeout'       => ''
        ],

    
        // Default Path for cache on drive
        // Use fall PATH like /home/username/cache
        // Keep it blank and it will set it up automatically for you
        'path'          => '@ROOT/Storage/Cache',
    
        // Default will be good. It will create a path by PATH/securityKey
        'securityKey'   => 'lnf2016wf',
    
        // FallBack Driver
        // Example: in your code, you use memcached, apc, etc. but when you move your web hosting
        // the FallBack Driver will be used until the cache is completely set up
        'fallback'     => 'files',
    
        // .htaccess protect
        // Default is 'no'
        'htaccess'      => false,
    
        // Use 1 as normal traditional, 2 is phpfastcache as default, 3 is phpfastcache memory stable
        'caching_method'=> 2
    ]
];