<?php

return [
     // Global cookie variables
    'path'      => '/',
    'domain'    => '',
    'secure'    => false,
    'httponly'  => true
];