<?php

return [
    'dsn'           => 'mysql:host='.env('DB_HOST', 'localhost').';port='.env('DB_PORT', 3306).';dbname='.env('DB_NAME', 'default'),
    'user'          => env('DB_USERNAME', 'root'),
    'password'      => env('DB_PASSWORD', ''),
    'settings'      => [
        'charset'   => 'utf8',
        'queries'   => []
    ],
    'classname'     => '\\Propel\\Runtime\\Connection\\ConnectionWrapper',
    'model_paths'   => [
        0   => 'src',
        1   => 'vendor'
    ]
];