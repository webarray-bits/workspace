<?php

return [
    // Options
    // Mailgun
    // Sendmail
    // ToDisk
    // SMTP
    'Driver'        => env('MAIL_DRIVER', 'ToDisk'),
    
    // Mail configuration
    'Configuration' => [
        'Encoding'      => env('MAIL_ENCODING', 'UTF-8'),
        'Host'          => env('MAIL_HOST', ''),
        'Username'      => env('MAIL_USERNAME', ''),
        'Password'      => env('MAIL_PASSWORD', ''),
        'SMTPDebug'     => env('MAIL_SMTPDEBUG', 0),
        'SMTPAuth'      => env('MAIL_SMTPAUTH', true),
        'SMTPSecure'    => env('MAIL_SMTPSECURE', 'tls'),
        'Port'          => env('MAIL_PORT', 587),
        'From'          => env('MAIL_FROM', 'noreply@graafschapcollege.nl'),
        'FromName'      => env('MAIL_FROMNAME', 'Graafschap Mailer'),
        'WordWrap'      => env('MAIL_WORDWRAP', 5)
    ]
];