<?php

return [
    // Specifies the path to the templates folder for Twig
    'Dir'           =>  '@ROOT/Resources/Views',
    'Extensions'    => [
        \Framework\Views\Extensions\GlobalExtension::class,
        \Framework\Views\Extensions\UserExtension::class,
    ]
];