<?php

namespace App\Middleware;

use Framework\Facades\Response;
use Framework\Facades\User;
use Framework\Middleware\Middleware;

class RequireAuth implements Middleware
{
    public static function run($role = null): int
    {
        if ( ! User::loggedIn() ) {
            response()->error(405, 405);
            return Middleware::Abort;
        }

        // The Administrator may access all contents
        if ( $role == null || User::hasRole($role) )
            return Middleware::Success;

        response()->error(405, 405);
        return Middleware::Abort;
    }
}