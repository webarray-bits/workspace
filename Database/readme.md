## What is this directory?

This directory contains all things database within the framework.
The following files are of importance:
- config.php: Automatically generated config file. Made with the command 'php frame db config:convert'.
- propel.php: The configuration file for the database.
- propel.php.dist: User specific database info. This is used for development, and contains user-local information.
- schema.xml: This xml file contains the database definitions as defined by Propel.

## How do I edit and / or update the database?

To edit the database schema, simply modify the schema.xml file.
To then update the database, the following command can be run. Please note that the command prompt should be in the Database directory, not the root of the project.
This will CLEAR the database of any data:

'php frame db renew'

If, for some reason, there is no symlink to the frame file within the Database directory, the following command can be run:

'php ..\frame db renew'

In the background, the follow Propel-commands are run:

'
sql:build
sql:insert
model:build
'

