<?php

use Framework\Kernel;

require_once __DIR__ . '/../vendor/autoload.php';

(new Kernel)->boot(realpath(__DIR__ . '/../'), false);